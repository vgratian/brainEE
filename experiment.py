
import sys
import os
import multiprocessing
from time import time
from itertools import combinations
from corpus import Corpus
from featurer import Featurer
from question import Question
from mc_perceptron import mcPerceptron

bold = '\033[1m'
unbold = '\033[0m'


class Experiment(object):
    """
    A fancy demonstration of how Braint can be used. Change variables under
    Configuration to try it out differently.

    IMPORTANT: Don't forget to remove output files, if you already have run Braint.
    E.g: run `rm experiment_*` in your terminal
    """

    def __init__(self, mode, nodes=None, params=None, options=None):

        # Default values for required params
        self.__name = 'BrainEE'
        self.__experiment_modes = {
                    'train': self.train,
                    'train_and_test': self.train_and_test,
                    'test': self.test,
                    'test_demo': self.test_demo,
                    }
        self.__nodes = None
        self.__params = {
                    'multiprocess': False,
                    'model_name': None,  # autogenerated
                    'classes': [], # will be extracted from corpus
                    #'vote_method': 'weighted majority: vote *= (cred/MI))',
                    'vote_method': 'node-class activations weighted by node-class score',
                    #'train_data': 'data/train_filtered',
                    #'train_data': 'data/train.txt',
                    'train_data': 'data/XXXStest',
                    #'test_data':'data/test_filtered',
                    #'test_data':'data/testwithoutlabels.txt',
                    'test_data':'data/XXXStest',
                    'data_size': {'train':0, 'test':0},
                    'epochs': 10,
                    'randomize_learning_rate': False,
                    'learning_rate_FN' : [0.5, 1.5],
                    'learning_rate_FP' : [0.1, 0.8],
                    'learning_rate_reduce' : [1, 10],
                    'feature_sets': (
                            '1GR',      # token unigram
                            '2GR',      # token bigram
                            '3GR',      # token trigram
                            '3GR-S1',   # token trigram
                            '4GR-S1',   # token skip-one-tetragram
                            '1CH',      # char unigram
                            '2CH',      # char bigram
                            '3CH',      # char trigram
                            'POS',      # Part-of-speech tags
                            #'SENTA'     # Negativity score for n-grams
                            ),
                    'score': 'frequency',
                    'calibrate_activations': True,
                    'weight_activations': True,
                    'node_count': 0,    # added by system
                    'reduce_nodes_to': False,
                    'reduce_from_epoch': 60,
                    'distributed_training': False,
                    'divisor': 1.5,
                    'print_results': True,
                    'print_plot': False,
                    'print_distribution': True,
                    'print_progressbar': True,
                    'load_model': False,
                    'save_model': False,
                    'save_credibility': True,
                    'save_test_predictions': True,
                    'save_results': True,
                    'save_mutual_information': True,
                    'save_plot': True,
                    'save_in_directory': None  # autogenerated
                    }
        self.__token_options = {
                    'addit_mode':True,
                    'lowercase':False,
                    'stem':False,
                    'replace_emojis':False,
                    'replace_num':False,
                    'remove_stopw':False,
                    'remove_punct':False
                    }
        self.name = None
        self.nodes = None
        self.params = None
        self.token_options = None

        self.parse_arguments(mode, nodes, params, options)

        self.__experiment_modes[mode]()


    def train_and_test(self):

        begin = time()

        self.print_intro()

        # Initialize corpora
        trainC = Corpus( fp=self.params['train_data'],
                         name='train',
                         print_distribution=self.params['print_distribution'])

        self.params['classes'] = trainC.get_labels()

        testC = Corpus(  fp=self.params['test_data'],
                         name='test',
                         print_distribution=self.params['print_distribution'])


        # Set corpora size
        self.params['data_size']['train'] = trainC.get_size()
        self.params['data_size']['test'] = testC.get_size()

        init_time = time() - begin
        print('\n{}Runtime Corpus initialization: {} s{}'.format(bold, \
            round(init_time, 3), unbold))

        self.print_tokenizing_options()

        # Extract features from corpora
        print('Extracting features from test data:')
        testF = Featurer( corpus=testC,
                          params=self.params,
                          token_options=self.token_options,
                          testset=True )

        print('\nExtracting features from train data:')
        trainF = Featurer( corpus=trainC,
                           params=self.params,
                           token_options=self.token_options,
                           testset=False,
                           filter=testF.get_feature_labels() )


        fext_time = time() - (begin + init_time)
        print('\n{}Runtime feature extraction: {} s (total time: {} s){}'.format( \
            bold, round((fext_time), 3), round((time()-begin),3), unbold ))

        print('\nTraining and testing {} model(s)...\n'.format(len(self.nodes)))

        jobs = []
        for nodes in self.nodes:
            self.params['node_count'] = len(nodes)
            self.params['model_name'] = self.name+'_'+ str(self.nodes.index(nodes))
            model = mcPerceptron(
                        nodes,
                        self.params,
                        self.token_options,
                        testF.get_feature_labels()
                        )
            if self.params['multiprocess']:
                p = multiprocessing.Process(target = model.train_and_test, args=(trainC, testC))
                p.start()
                jobs.append(p)
            else:
                model.train_and_test(trainC, testC)

        if self.params['multiprocess']:
            for p in jobs:
                p.join()

        end_t = time()

        print('\n{}Runtime training & testing: {} s (total: {} s){}'.format(bold, \
            round((end_t-(begin+fext_time+init_time)),3),
            round((time()-begin),3), unbold))
        print('\nFinalized prediction and evaluation.')

        print('Total runtime: {} s.'.format(round(time()-begin,3)))


    def train(self):
        begin = time()
        self.print_intro()

        # Initialize corpus
        if self.params['print_distribution']:
            print('Class distribution in TRAIN data:')
        train_corpus = Corpus(self.params['train_data'],self.params['print_distribution'])

        print('\nTokenizing tweets with options:\n{}'.format('\n'.join([' ' \
            '{}:\t\t{}'.format(o,v) for o,v in zip(self.token_options.keys(), \
            self.token_options.values())])))

        # Extract features
        print('\nExtracting features from TRAIN data:')
        features_train = Featurer(train_corpus, self.params, self.token_options)

        print('\nTraining model...\n')

        model = mcPerceptron(
                    self.params, \
                    self.token_options, \
                    train_corpus.get_all_feature_names()
                    )
        model.train(train_corpus)

        print('\nTraining model completed.')

        if self.params['save_model']:
            print('Model saved as {}'.format(self.params['save_model']))

        print('Total runtime: {} s.'.format(round(time()-begin,3)))


    def test(self):

        begin = time()

        self.print_intro()

        testC = Corpus(  fp=self.params['test_data'],
                         name='test',
                         print_distribution=self.params['print_distribution'])

        self.params['data_size']['test'] = testC.get_size()
        self.params['data_size']['train'] = 0

        print('Extracting features from test data:')
        testF = Featurer( corpus=testC,
                          params=self.params,
                          token_options=self.token_options,
                          testset=True )

        self.params['model_name'] = self.name+'_0'
        model = mcPerceptron(
                    self.nodes[0],
                    self.params,
                    self.token_options
                    )

        model.test_model(testC)

        print('\nModel evaluation completed.')

        if self.params['save_test_predictions']:
            print('Predicted labels saved as {}'.format(self.params['save_test_predictions']))

        if self.params['save_results']:
            print('Evaluation results saved as {}'.format(self.params['save_results']))

        print('Total runtime: {} s.'.format(round(time()-begin,3)))


    def test_demo(self):
        self.model = mcPerceptron(
                    self.params, \
                    self.token_options
                    )
        self.model.load_model()


    def predict(self, text):
        tweet = Tweet(text)
        featurer = Featurer(None, self.params, self.token_options)
        features = featurer.extract_features(tweet)
        emotion = self.model.predict(features, tweet)
        return emotion[0][0]


    def parse_arguments(self, mode, nodes=None, params=None, options=None):

        # DEBUG Merge with the rest of code!!
        self.nodes = nodes if nodes else self.__nodes
        self.params = params if params else self.__params
        self.token_options = options if options else self.__token_options

        # Verify experiment params
        assert all([True if p in self.__params else False for p in self.params]), \
            'Invalid params. Expected:\n {}'.format(',\n '.join(self.__params.keys()))

        # Verify tokenization options
        assert all([True if o in self.__token_options else False for o in self.token_options]), \
            'Invalid token params. Expected:\n {}'.format(',\n '.join(self.__token_options.keys()))
        #TODO if not all options are provided, take from default

        # Verify mode is valid
        assert mode in self.__experiment_modes, 'Invalid mode "{}". Expected ' \
            'one of: [{}]'.format(mode, ', '.join(self.__experiment_modes))

        self.name = self.generate_model_name()
        print(bold, self.name.upper(), unbold)

        self.params['feature_sets'] = {fset for node in self.nodes for fsets in node for fset in fsets}

        if any([self.params['save_model'], \
                self.params['save_test_predictions'], \
                self.params['save_results'], \
                self.params['save_plot'], \
                self.params['save_mutual_information'] ]):
            self.params['save_in_directory'] = self.make_results_directory()


    def generate_model_name(self):
        base_name = self.__name
        expn = 'exp{}'.format(self.get_experiment_number())
        epochs = 'ep{}'.format(self.params['epochs'])
        datasets = 'dS-{}-{}'.format(
                               self.params['train_data'].split('/')[-1],
                               self.params['test_data'].split('/')[-1] )

        return '_'.join([base_name, expn, epochs, datasets])


    def get_experiment_number(self):

        fp = 'data/EXPERIMENT_NUMBER'
        self.experiment_number = 0

        if os.path.isfile(fp):
            with open(fp, 'r') as f:
                self.experiment_number = int(f.read()) + 1

        with open(fp, 'w') as f:
            f.write(str(self.experiment_number))

        return self.experiment_number


    def make_results_directory(self):
        path = 'results/' + self.name + '/'
        if os.path.exists(path):
            print('WARNING. Folder [{}] already exists. Files might be '
                'overwritten'.format(path))
        else:
            os.makedirs(path)
        return path


    def print_tokenizing_options(self):
        if self.token_options:
            print('\nTokenizing tweets with options:\n{}'.format \
                ('\n'.join([' {}:\t\t{}'.format(o,v) for o,v in \
                    zip(self.token_options.keys(), self.token_options.values())])))
            print()


    def print_intro(self):
        # Print info about params
        print('Starting BrainEE with params:\n{}'.format('\n'.join([' ' \
            '{}:\t\t{}'.format(p,v) for p,v in zip(self.params.keys(), \
            self.params.values())])))


if __name__ == "__main__":

    """
    fsets = ('1GR', '2GR', '3GR', '4GR-S1', '1CH', '2CH', '3CH', 'POS', 'SENTA')
    fset_combinations = []
    nodes = []
    m = 18  # number of nodes in each ensemble

    for x in combinations(fsets, 4):    # Gives us 126 nodes
        fset_combinations.append(x)

    count = len(fset_combinations)

    for i in range(0, count, m):
        nodes.append(fset_combinations[i:i+m])

    # nodes is now a tuple of list, where
    # each lists contains tuples of fsets, i.e. each tuple is a node
    nodes = tuple(nodes)
    print(nodes)
    """

    custom_nodes = ([

            ('1GR', '2GR',),
            ('2GR', 'SENTA',),
            ('1GR', '2GR', '1CH',),
            ('1GR', '2GR', '3CH',),
            ('1GR', '2GR', 'SENTA',),
            ('2GR', '3CH', 'SENTA',),
            ('2GR', '4GR-S1', '3CH',),
            ('1GR', '2GR', 'POS',),
            ('1GR', '3GR', '3CH', 'POS',),
            ('1GR', '2GR', '3CH', 'SENTA',),
            ('2GR', '4GR-S1', '3CH', 'POS',),

            ],)


    exp = Experiment('train_and_test', custom_nodes)
    #exp.predict(text)
