
from operator import itemgetter
from random import uniform, randrange

class Node(object):

    def __init__(self, params:dict, fsets:tuple, features:dict):

        self.params = params
        self.classes = params['classes']
        self.fsets = fsets
        self.weights = {c:{fset:{} for fset in fsets} for c in self.classes}
        self.avg_weights = {c:{fset:{} for fset in fsets} for c in self.classes}

        # Initialize random weights
        for c in self.classes:
            for fset in fsets:
                for f in features[fset]:
                    w = uniform(-1,1)
                    self.weights[c][fset][f] = w
                    self.avg_weights[c][fset][f] = w

        if self.params['randomize_learning_rate']:
            self.learning_rate_FP = round(uniform(*self.params['learning_rate_FP']),2)
            self.learning_rate_FN = round(uniform(*self.params['learning_rate_FN']),2)
            self.learning_rate_reduce = randrange(*self.params['learning_rate_reduce'])
        else:
            self.learning_rate_FP = 0.8
            self.learning_rate_FN = 0.4
            self.learning_rate_reduce = 2


    def predict(self, features, test_mode=False):

        weights = self.avg_weights if test_mode else self.weights
        activations = {}

        for c in self.classes:
            activation = 0
            for fset in self.fsets:
                for f in features[fset]:
                    if f in weights[c][fset]:
                        activation += ( features[fset][f] * weights[c][fset][f] )
            activations[c] = activation

        # prediction is class with highest activation
        prediction = sorted(activations.items(), key=itemgetter(1), reverse=True)[0][0]
        self.last_prediction = prediction # TODO why do we need this?

        if self.params['calibrate_activations']:
            s = abs(sum(activations.values()))
            weighted_activations = { c:(activations[c]/s) for c in activations }
            return prediction, weighted_activations
        return prediction, activations


    def update_weights(self, features, prediction, gold_label, r):
        """
        If we are here, it means prediction != gold_label.
        """

        # learning reate for FPs (adding more weight)
        L = self.learning_rate_FP if prediction != 'others' else self.learning_rate_FP / self.learning_rate_reduce
        l = self.learning_rate_FN if gold_label != 'others' else self.learning_rate_FN / self.learning_rate_reduce

        for fset in self.fsets:
            if fset in features:
                for f in features[fset]:
                    if f in self.weights[prediction][fset]:

                        # We will punish FPs harder than FNs
                        Z = features[fset][f] * L
                        z = features[fset][f] * l

                        # Enforce false negative class
                        self.weights[gold_label][fset][f] += z
                        self.avg_weights[gold_label][fset][f] += ( z * r )
                        # Punish false positive class
                        self.weights[prediction][fset][f] -= Z
                        self.avg_weights[prediction][fset][f] -= ( Z * r )


    def get_last_prediction(self):

        return self.last_prediction
