class Question(object):
    """ A datastructure to represent a question object.
    """

    def __init__(self, text:str, id:int, gold_label:str=None, pred_label:str=None):
        """ Inits Question object.

            Args:
                text: the text of this Tweet as a string.
                (optional) gold_label: the gold label for this tweet as a string.
                (optional) pred_label: the predicted label for this tweet as a string.
        """
        self.__id = id
        self.__text = text
        self.__pred_label = pred_label
        self.__gold_label = gold_label
        self.__votes = None
        self.__owners = []
        self.__features = {}


    def set_features(self, features):
        """ Set the features for this Tweet.

            Args:
                features: a dictionary which maps from featurenames (strings) to values
        """
        #self.del_text()
        self.__features = features


    def set_pred_label(self, pred_label : str):
        """ Sets the predicted label of this tweet.

            Args:
                pred_label: the predicted label for this tweet as a string.
        """
        self.__pred_label = pred_label


    def set_votes(self, votes : dict):
        """
            Args: Dictionary where key is the neuron (or feature set) and the
            value is the vote.
        """
        self.__votes = votes


    def set_owner(self, owner:str):
        self.__owners.append(owner)


    def get_features(self):
        """ Returns the features for this Tweet.

            Returns:
                a dictionary which maps from feature names (strings) to values
        """
        return self.__features


    def get_text(self):
        """ Gets the text of this tweet.

            Returns:
                The tweet text as a String.
        """
        return self.__text if self.__text else None


    def get_pred_label(self):
        """ Gets the predicted label of this tweet.

            Returns:
                The predicted label as a String.
        """
        return self.__pred_label


    def get_gold_label(self):
        """ Gets the gold label of this tweet.

            Returns:
                The gold label as a String.
        """
        return self.__gold_label

    def get_id(self):
        return self.__id

    def get_votes(self):
        return self.__votes


    def get_owners(self):
        return self.__owners


    def del_text(self):
        if self.__text:
            del self.__text
            self.__text = None


    def del_features(self):
        if self.__features:
            del self.__features


    def __str__(self):
        """ Generates a string representation of this tewwt object.

            Returns:
                A string representation of this tweet object,
                containg the tweet text, the predicted label and the gold label.
        """
        return '\n'.join( [ '------',
                            str(self.__id),
                            self.__text if self.__text else '<MISSING TEXT>',
                            str(self.__gold_label),
                            #str(self.__features),
                            '------'
                           ] )
