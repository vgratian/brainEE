import json
import sys

from time import time
from math import log2
from random import uniform
from operator import itemgetter
from node import Node

sys.path.append('evaluator/')
from result import Result
from scorer import Scorer

sys.path.append('../Braint/')
from utils.progress_bar import print_progressbar


class mcPerceptron(object):
    """
    Ensemble Perceptron model of binary classification.

    Nodes are trained on different feature sets and a prediction is made based
    on majority voting. Each model is also assigned a credibility score which
    gives more/less weights to their vote.

    Results are (but don't have to) printed on screend and / or saved to a file.
    Optionally prints a plot to show the results.

    Currently tested:
        Train and test

    Not tested: TODO!
        Save model
        Load model

        Args:
            params: dict
            token_options: options for tokenizationn (only used for the plot)
            feature_names: list containing all names of features that are used.
    """

    def __init__(self, nodes:list, params:dict, token_options:dict, feature_names:dict ):

        self.params = params
        self.token_options = token_options

        # Tuple
        self.feature_sets = params['feature_sets']
        # List
        self.classes = params['classes']
        # Each node is a tuple of feature sets, concatenate those into names
        self.nodes = {}
        for n,i in zip(nodes, range( len(nodes) ) ):
            # name for each node: index + concactination of feature sets
            name =  str(i) + '_' + '_'.join(n)
            self.nodes[name] = Node(params, n, feature_names)
        self.nodesL = sorted(list(self.nodes.keys()))

        # Initialize metaclassifier weights
        """
        self.metaclassifier = {}
        self.metaclassifier_avg = {}
        for c in self.classes:
            self.metaclassifier[c] = {str(n+'_'+C):1 for n in self.nodes for C in self.classes}
            self.metaclassifier[c]['<BIAS>'] = 1
            self.metaclassifier_avg[c] = {str(n+'_'+C):1 for n in self.nodes for C in self.classes}
            self.metaclassifier_avg[c]['<BIAS>'] = 1
        """

        self.num_steps = 0  # Used to average weights
        self.curr_step = 0  # Used to average weights
        self.curr_epoch = None # Used to calculate mutual information
        self.final_scores = None # Used for reporting final performance of models
        self.current_scores = None

        # Set initial credibility ranking for each perceptron
        self.credibility = {node:{c:1 for c in self.classes} for node in self.nodes}
        self.avg_precision = {node:{c:1 for c in self.classes} for node in self.nodes}

        # Mutual information, n x n matrix
        self.mutual_information = {n:{n:0 for n in self.nodes} for n in self.nodes}
        self.sumMI = {n:0 for n in self.nodes}
        self.netMI = {n:0 for n in self.nodes}
        # Probabilities, we will use them to calculate mutual information
        self.jointP = {n:{} for n in self.nodes}
        for n in self.jointP:
            for m in self.jointP:
                self.jointP[n][m] = {'count':0, 'total':0}

        # For each node we need to calculate the following information:
        # C(n=1)        - number of times n voted 1
        # C(n=x)        - total number of times n voted
        # C(n=1 ; m=1)  - number of times two models voted 1
        # C(n=x ; m=x)  - total number of times both models voted


    def train_and_test(self, train_corpus, test_corpus):

        if self.params['print_results'] or self.params['save_results'] \
            or self.params['print_plot']:
            result = Result(self.nodesL, self.params)
        else:
            result = None

        if self.params['distributed_training']:
            self.assign_datasets(train_corpus)

        self.train(train_corpus, test_corpus, result)

        # The scenario when we want to train model and label unlabeled test data
        if test_corpus and not self.params['print_results']:
            self.test(test_corpus, test_mode=True)

        self.print_mutual_information()

        if self.params['print_distribution']:
            test_corpus.print_distribution(predictions=True)

        if self.params['save_test_predictions']:
            test_corpus.write_predictions(self.params)
            test_corpus.write_node_predictions(self.params, self.nodes)

        if self.params['save_model']:
            self.save_model()

        if self.params['save_credibility']:
            self.save_credibility()

        if self.params['print_plot']:
            result.draw_graph()

        if self.params['save_plot']:
            result.draw_graph(save=True)


    def train(self, train_corpus, test_corpus=None, result=None):
        """ Train the model. Optionally writes weights and accuracy into files.
            Args:
                train_corpus:   iterable corpus with samples
                test_corpus:    corpus to test on model

            Args from self.params:
                'epochs'        training iterations
                'print_results' print evaluation results: Boolean
                'save_results'  save evaluation results: String or None
                'save_model'    save weights into file: String (filename)
        """
        epochs = self.params['epochs']
        self.num_steps = epochs * train_corpus.get_size()
        self.curr_step = self.num_steps
        self.curr_epoch = 0
        accuracy = 0
        if self.params['print_progressbar'] and not self.params['print_results']:
            progress = 0
            print_progressbar(progress, self.num_steps)

        for e in range(epochs):
            self.curr_epoch += 1
            correct = 0
            train_corpus.shuffle()
            for x in train_corpus:
                gold_label = x.get_gold_label()            # String
                owners = x.get_owners()                    # List
                features = x.get_features()                # Dict
                prediction, votes = self.predict(features, x)   # Int, Dict
                self.update_weights(features, votes, owners, gold_label)
                self.curr_step -= 1
                # Keep count of correct predictions
                correct += 1 if prediction==gold_label else 0

                if self.params['print_progressbar'] and not self.params['print_results']:
                    progress += 1
                    print_progressbar(progress, self.num_steps)

            # Calculate accuracy score for current iteration
            # This score shows how the model is converging
            accuracy = round((correct / train_corpus.get_size()), 2)

            self.calculate_mutual_information()

            # The scenario when we want to test on labeled test data
            if test_corpus and self.params['print_results']:
                self.test(test_corpus, test_mode=True)
                scores = Scorer(test_corpus, self.nodesL)
                self.current_scores = scores

                if self.curr_epoch == self.params['epochs']:
                    self.final_scores = scores

                if self.params['print_results']:
                    result.show(scores, accuracy)

                if self.params['save_results']:
                    result.write(scores, accuracy)

                self.update_credibility()

            # Start reducing nodes after 10th iteration
            if self.params['reduce_nodes_to']:
                if e > self.params['reduce_from_epoch']:
                    if self.params['reduce_nodes_to'] < len(self.nodes):
                        self.print_credibility()
                        loser = self.get_lowest_rank_node()
                        del self.nodes[loser]
                        print('\nDeleted [{node}] with scores [{scores}]\n'.format \
                                ( node=loser, scores=', '.join(['{}: {}'.format(c,s) for c,s in self.credibility[loser].items()]) ) )
                        if result:
                            result.remove_node(loser)


    def test(self, test_corpus, test_mode=False):
        """
        Will use custom weights is passed by argument, otherwise class weights
        will be used.

        Args:
        examples: corpus (iterable) containing Tweets
        weights: (optional) use custom weights
        """
        for sample in test_corpus:
            self.predict(sample.get_features(), sample, test_mode)


    def test_model(self, test_corpus):

        self.load_model()
        if self.params['print_results'] or self.params['save_results']:
            result = Result(self.nodesL, self.params)

        self.test(test_corpus)

        scores = Scorer(test_corpus, self.nodesL)

        if self.params['print_results']:
            result.show(scores)

        if self.params['save_results']:
            result.write(scores)


    def assign_datasets(self, corpus):
        """
        Assign different subsets of the dataset to our models. Each model gets
        about 25% of the total dataset by default.
        TODO: Customize this ratio for each node?
        """
        divisor = self.params['divisor'] if self.params['divisor'] else 4
        N = corpus.get_size()
        S = int( N / divisor )

        for node in self.nodes:
            corpus.shuffle()
            for x in corpus.corpus[:S]:
                x.set_owner(node)


    def predict(self, features, example, test_mode=False):
        """ Returns a prediction for the given features. Calculates activation
            for each class and returns the class with the highest activation.
            Args:
                features: dictionary containing features and values for these
                example: tweet object for which prediction is made
            Returns:
                a tuple containg (predicted_label, activation score)
        """

        votes = {}
        all_activations = {c:0 for c in self.classes}

        for node in self.nodes:
            votes[node], activations = self.nodes[node].predict(features, test_mode)
            for c in activations:
                if self.params['weight_activations']:
                    all_activations[c] += ( activations[c] + ( activations[c] * self.credibility[node][c] ) )
                else:
                    all_activations[c] += activations[c]

        self.update_joint_probabilities(votes)

        final_prediction = self.ansamble_activation(all_activations)
        #final_prediction = self.ensemble_prediction(all_activations, example.get_gold_label(), test_mode)

        example.set_pred_label(final_prediction)
        example.set_votes(votes)
        return final_prediction, votes


    def ensemble_prediction(self, node_activations, gold_label, test_mode=False):

        weights = self.metaclassifier_avg if test_mode else self.metaclassifier

        self.features = {str(n+'_'+c):node_activations[n][c] for n in self.nodes for c in self.classes}
        self.features['<BIAS>'] = 1
        activations = {}

        for c in self.classes:
            activations[c] = sum([ (weights[c][f] * self.features[f]) for f in self.features])
        prediction = sorted(activations.items(), key=itemgetter(1), reverse=True)[0][0]

        return prediction


    def update_ensemble(self, prediction, gold_label):
        #lr = 0.1 if gold_label==1 else 0.02
        if prediction != gold_label:
            L = 0.8 if prediction != 'others' else 0.4
            l = 0.4 if gold_label != 'others' else 0.2
            r = (self.curr_step / self.num_steps) if self.curr_step !=0 else 0

            for f in self.features:
                Z = (self.features[f] * L)
                z = (self.features[f] * l)

                self.metaclassifier[gold_label][f] += z
                self.metaclassifier_avg[gold_label][f] += (z * r)

                self.metaclassifier[prediction][f] -= Z
                self.metaclassifier_avg[prediction][f] -= (Z * r)


    def ansamble_activation(self, activations):

        prediction = sorted(activations.items(), key=itemgetter(1), reverse=True)[0][0]

        return prediction


    def update_weights(self, features, votes, owners, gold_label):
        """ Updates the weights of the perceptron.
            Args:
                features:   iterable containg the names of the features of the
                            current example
                votes:      predictions of each node, dict:
                            key=node, value=prediction
                gold_label: the true label as a string
        """
        # Rate for averaging weights
        r = (self.curr_step / self.num_steps) if self.curr_step !=0 else 0

        nodes = owners if self.params['distributed_training'] else self.nodes
        for node in nodes:
            if node in self.nodes: # might be killed node
                prediction = votes[node]
                if prediction != gold_label:
                    self.nodes[node].update_weights(features, prediction, gold_label, r)


    def update_credibility(self):
        # update weights with latest f-micro score
        scores = self.current_scores
        for n in self.nodes:
            for c in self.avg_precision[n]:
                new_avg = (self.avg_precision[n][c] + scores.precision_labels[n][c]) / 2
                self.avg_precision[n][c] = round(new_avg, 4)
                self.credibility[n][c] = self.avg_precision[n][c]

        # Calibrate, so the creds for each class sum to 1
        #for c in self.classes:
        #    S = sum([self.avg_precision[N][C] for C in self.classes for N in self.nodes if C==c])
        #    for n in self.nodes:
        #        self.credibility[n][c] = round( (self.avg_precision[n][c] / S), 4)


    def update_joint_probabilities(self, votes):
        """
        Calculate probability that two models will vote "1". Based on pre-
        calculated counts. When n==m, we get the probability for the node
        itself.
        """
        for n in self.nodes:
            for m in self.nodes:
                if votes[n] == votes[m]:
                    self.jointP[n][m]['count'] += 1
                self.jointP[n][m]['total'] += 1


    def get_lowest_rank_node(self):

        node_rankings = {n:0 for n in self.nodes}

        for c in self.classes:
            ranking = [(self.credibility[n][c], n) for n in self.nodes]
            ranking = sorted(ranking, key=itemgetter(0))
            for r in ranking:
                rank = ranking.index(r)+1
                node = r[1]
                if rank > node_rankings[node]:
                    node_rankings[node] = rank

        # get node with lowest ranking
        loser = sorted(node_rankings.items(), key=itemgetter(1))[0][0]

        return loser


    def calculate_mutual_information(self):
        """
        Calculate mutual information between nodes based on probabilities to
        vote "1". Then calculate summed MI for each node and a net MI.

        NetMI is the MI of the node to itself (the information it has gained),
        minus the information it shares with others. So it is, hopefully,
        an indication how much information is exclusively in that node.
        """

        MI = {n:{m:None for m in self.nodes} for n in self.nodes}
        for n in self.nodes:
            for m in self.nodes:
                if MI[m][n]:
                    MI[n][m] = MI[m][n]
                else:
                    n_p = self.jointP[n][n]['count'] / self.jointP[n][n]['total']
                    m_p = self.jointP[m][m]['count'] / self.jointP[m][m]['total']
                    joint_p = self.jointP[n][m]['count'] / self.jointP[n][m]['total']
                    if (n_p * m_p) != 0:
                        MI[n][m] = round( (joint_p * log2( joint_p / (n_p * m_p))), 5 )
                    else:
                        MI[n][m] = 0
        self.mutual_information = MI

        # Calculate summed MI, i.e. shared information
        for n in self.nodes:
            smi = sum([self.mutual_information[n][m] for m in self.nodes if m!=n])
            smi = round(smi,2)
            self.sumMI[n] = smi

        # Calculate net MI, i.e. exclusive information
        for n in self.nodes:
            nmi = (self.mutual_information[n][n] - self.sumMI[n])
            nmi = round(nmi,2)
            self.netMI[n] = nmi


    def print_mutual_information(self):

        nodes = sorted(list(self.nodes.keys()))

        text = '\n'
        text += (' ' * 25) + '|'
        for n in nodes:
            text += '{}{}|'.format((' ' * (15 - len(n))), n)
        text += '\n'
        text += ('-' * 25) + '|'
        text += ('-' * 15 + '|') * (len(nodes)) + '\n'

        for n in nodes:
            text += '{}{}'.format(n, (' ' * (25-len(n)))) + '|'
            for m in nodes:
                mi = round(self.mutual_information[n][m],2)
                text += '{}{}'.format((' ' * (15-len(str(mi)))), mi) + '|'
            text += '\n'

        text += ('-' * 25) + '|'
        text += ('-' * 15 + '|') * (len(nodes)) + '\n'

        bold = '\033[1m'
        unbold = '\033[0m'

        # Add sum of MI for each node
        text += ' shared mi' + (' ' * 15) + '|'
        for n in nodes:
            text += '{}{}{}{}'.format(bold, (' ' * (15-len(str(self.sumMI[n])))), self.sumMI[n], unbold) + '|'
        text += '\n'

        # Add net sum, i.e. own MI - joint MIs
        text += ' exclusive mi' + (' ' * 12) + '|'
        for n in nodes:
            text += '{}{}{}{}'.format(bold, (' ' * (15-len(str(self.netMI[n])))), self.netMI[n], unbold) + '|'
        text += '\n'
        print(text)

        if self.params['save_mutual_information']:
            fp = self.params['save_in_directory'] + 'mutual_information'
            with open(fp, 'a') as w:
                w.write(text + '\n')
                print('MI scores saved as [{}]'.format(fp))

            fp = self.params['save_in_directory'] + 'summary_results'
            with open(fp, 'a') as w:
                w.write('Node\tF-micro\tP\tR\tF_Anger\tF_Sad\tF_Happy\tF_Other\tP_Anger\tR_Anger\tP_Sad\tR_Sad\tP_Happy\tR_Happy\tP_Other\tR_Other\n')
                ensemble = '{node}\t{f}\t{p}\t{r}\t{fa}\t{fs}\t{fh}\t{fo}\t{pa}\t{ra}\t{ps}\t{rs}\t{ph}\t{rh}\t{po}\t{ro}'.format(
                    node = 'ENSEMBLE',
                    f = round(self.final_scores.f_micro['ensemble'],3),
                    p = round(self.final_scores.precision['ensemble'],3),
                    r = round(self.final_scores.recall['ensemble'],3),
                    fa = round(self.final_scores.f_micro_labels['ensemble']['angry'],3),
                    fs = round(self.final_scores.f_micro_labels['ensemble']['sad'],3),
                    fh = round(self.final_scores.f_micro_labels['ensemble']['happy'],3),
                    fo = round(self.final_scores.f_micro_labels['ensemble']['others'],3),
                    pa = round(self.final_scores.precision_labels['ensemble']['angry'],3),
                    ra = round(self.final_scores.recall_labels['ensemble']['angry'],3),
                    ps = round(self.final_scores.precision_labels['ensemble']['sad'],3),
                    rs = round(self.final_scores.recall_labels['ensemble']['sad'],3),
                    ph = round(self.final_scores.precision_labels['ensemble']['happy'],3),
                    rh = round(self.final_scores.recall_labels['ensemble']['happy'],3),
                    po = round(self.final_scores.precision_labels['ensemble']['others'],3),
                    ro = round(self.final_scores.recall_labels['ensemble']['others'],3)
                )
                w.write(ensemble + '\n')
                for n in self.nodesL:
                    if n in self.nodes:
                        results = '{node}\t{f}\t{p}\t{r}\t{fa}\t{fs}\t{fh}\t{fo}\t{pa}\t{ra}\t{ps}\t{rs}\t{ph}\t{rh}\t{po}\t{ro}'.format(
                            node = n,
                            f = round(self.final_scores.f_micro[n],3),
                            p = round(self.final_scores.precision[n],3),
                            r = round(self.final_scores.recall[n],3),
                            fa = round(self.final_scores.f_micro_labels[n]['angry'],3),
                            fs = round(self.final_scores.f_micro_labels[n]['sad'],3),
                            fh = round(self.final_scores.f_micro_labels[n]['happy'],3),
                            fo = round(self.final_scores.f_micro_labels[n]['others'],3),
                            pa = round(self.final_scores.precision_labels[n]['angry'],3),
                            ra = round(self.final_scores.recall_labels[n]['angry'],3),
                            ps = round(self.final_scores.precision_labels[n]['sad'],3),
                            rs = round(self.final_scores.recall_labels[n]['sad'],3),
                            ph = round(self.final_scores.precision_labels[n]['happy'],3),
                            rh = round(self.final_scores.recall_labels[n]['happy'],3),
                            po = round(self.final_scores.precision_labels[n]['others'],3),
                            ro = round(self.final_scores.recall_labels[n]['others'],3)
                        )
                    else:
                        results = '{node}\t\{f}\t{p}\t{r}\t{mi}\t{a}\t{s}\t{h}\t{o}'.format(
                            node = n,
                            f = '--',
                            p = '--',
                            r = '--',
                            mi = '--',
                            a = round(self.avg_precision[n]['angry'],3),
                            s = round(self.avg_precision[n]['sad'],3),
                            h = round(self.avg_precision[n]['happy'],3),
                            o = round(self.avg_precision[n]['others'],3),
                        )
                    w.write(results + '\n')
                print('Summarized results saved as [{}]'.format(fp))


    def save_model(self):
        fp = self.params['save_in_directory'] + self.params['model_name']
        with open(fp+'_avg.brainq', 'w') as w:
            w.write(json.dumps(self.avg_weights) + '\n')
            print('Model saved as [{}]'.format(fp))


    def save_credibility(self):
        fp = self.params['save_in_directory'] + self.params['model_name'] +'.cred'
        with open(fp, 'w') as w:
            w.write(json.dumps(self.credibility) + '\n')
            print('Credibility weights saved as [{}]'.format(fp))


    def load_model(self):
        with open(self.params['load_model'], 'r') as w:
            self.weights = json.load(w)


    def print_credibility(self):

        classes = ['angry', 'sad', 'happy', 'others']

        print()
        print(' ' * 25, end='|')
        for c in classes:
            print(' ' * (10 - len(c) ), end='')
            print(c, end='|')
        print('  FP, FN, reduce', end='')
        print()

        print('-' * 25, end='|')
        print((('-' * 10) + '|') * 4, end='')
        print('-' * 15)

        for node in self.nodes:
            print(node, end='')
            print(' ' * (25 - len(node)), end='|')
            for c in classes:
                score = self.credibility[node][c]
                score = str(score)
                print(' ' * (10 - len(score)), end='')
                print(score, end='|')
            print('  ', end='')
            print(self.nodes[node].learning_rate_FP, end=' ')
            print(self.nodes[node].learning_rate_FN, end=' ')
            print(self.nodes[node].learning_rate_reduce)

        print('-' * 25, end='|')
        print((('-' * 10) + '|') * 4, end='')
        print('-' * 15)


    def print_weights(self):
        """
        Only meant for debugging
        """
        print('=' * 50)
        print('\t\tENSEMBLE\tENSEMBLE AVERAGED')

        for f in self.ensemble:
            print(f.upper() + (' ' * (10-len(f))), end = '\t')
            print(round(self.ensemble[f],3), end='\t')
            print(round(self.ensemble_averaged[f],3))
        print('=' * 50)
