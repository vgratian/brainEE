import sys
from math import log10
from nltk import pos_tag
from tokenizer import Tokenizer
from corpus import Corpus
sys.path.append('../Braint/')
from utils.progress_bar import print_progressbar
sys.path.append('sent_classifier')
from sent_classifier import senti_classifier


class Featurer():
    """
    :Featurer:

    Extracts features from tweet collection. Features can be both unigram and
    bigram tokens. Feature values can be calculated in 4 methods (see under
    Options). An additional feature, '<BIAS>' (whose value is always 1) is
    added to the features, since those are intended to be used by a Perceptron.

    Don't wait for any return values, features are sent to the Corpus and each
    corresponding Tweet instance

    :Paramteres:
        - corpus -- an instance of class Corpus
        - token_options -- parameters that will be sent to Tokenizer, dict
        - grams -- number of grams to extract: 1 or 2 or both, tuple of ints
        - type -- feature type, str, select one from:
            -- binary
            -- count
            -- frequency
            -- tf_idf

    :Output:
    Directly fed back into the corpus and its tweet objects. See the following
    methods to access the features:
        - corpus.get_all_features() -- list of all feature labels in corpus
        - tweet.get_features() -- dict of features and values for each tweet

    :Usage:
        f = Featurer(corpus, PARAMETERS)
    :Example usage:
        f = Featurer(train_corpus, token_options=None, grams=(1,2), type='frequency')

    """

    def __init__(self,
                corpus:Corpus,
                params:dict,
                token_options:dict,
                testset:bool=False,
                filter:dict=None ):

        """
        Expected parameters:

        """
        self.corpus = corpus if corpus else None
        self.corpus_size = corpus.get_size() if corpus else 0
        self.token_options = token_options
        # Tuple!
        self.feature_sets = params['feature_sets']
        self.print_progressbar = params['print_progressbar']
        #if not testdata:
        # Dict of sets. Dict keys are feature_sets / neurons
        self.feature_labels = {fset:{} for fset in self.feature_sets}
        self.size = {fset:0 for fset in self.feature_sets}
        self.filter = filter if filter else None
        self.testdata = testset

        self.extract()
        #if not testdata:
        #    self.shrink_features()
        self.normalize_sample_features()


    def extract(self):
        """
        Main routine the performs some basic calculations before feature vectors
        are extracted:
        (1) counts corpus size,
        (2) extracts list of all terms from colection (a.k.a. "feature labels")

        Extracts features for each tweet and adds to corresponding Tweet object.
        If no or invalid type is given as parameter, prints a soft warning.
        """

        # Main job: extract features and print progressbar
        if self.print_progressbar:
            progress = 1
            print_progressbar(progress, self.corpus_size)

        # For each sample we get a dict of dicts
        for sample in self.corpus:
            features = self.extract_features(sample)
            sample.set_features(features)

            # Add sample features to global feature labels
            # We calculate here document frequency of each feature
            #if self.testdata:
            for fset in features:
                for f in features[fset]:
                    self.feature_labels[fset][f] = self.feature_labels[fset].get(f,0) + 1

            if self.print_progressbar:
                print_progressbar(progress, self.corpus_size)
                progress += 1

        # Add feature labels to corpus
        #self.corpus.set_all_feature_names(self.feature_labels) #DEPRECATED
        #if not self.testdata:
        print()
        print('Total features extracted:')
        for fset in self.feature_labels.keys():
            self.size[fset] = len(self.feature_labels[fset])
            print('{}: \t {}'.format(fset, self.size[fset]))


    def shrink_features(self):
        new_features = {fset:set() for fset in self.feature_sets}
        print('------------------------------------------------------------------------------------')
        print('FEAT\t\t\tCOUNT\t\tFREQ')
        print('------------------------------------------------------------------------------------')
        for fset in self.feature_labels.keys():
            low = 0
            high = 0
            N = self.corpus_size
            removed = 0
            #subsum = sum(self.feature_labels[fset].values())
            for feat,count in self.feature_labels[fset].items():
                freq = count / N
                #if count > 10000:
                #    print('{}\t\t\t{}\t\t{}'.format(feat,count,freq))
                if freq > 0.99 or freq < 0:
                    removed += 1
                    if freq > 0.99:
                        high += 1
                    elif freq < 0:
                        low +=1
                else:
                    new_features[fset].add(feat)
            # Add fset bias
            new_features[fset].add('<BIAS>')
            print("Removed features from {}: {} (T: {}): (L:{}, H:{})".format \
                (fset, removed, self.size[fset], low, high))

        # delete old feature labels
        #del self.feature_labels
        del self.feature_labels
        self.feature_labels = new_features
        #print('Size o', len(self.feature_labels))


    def normalize_sample_features(self):

        filter = self.filter if self.filter else self.feature_labels

        max = {fset:0 for fset in self.feature_sets}
        min = {fset:1000 for fset in self.feature_sets}
        for sample in self.corpus:
            old_features = sample.get_features()
            new_features = {fset:{} for fset in old_features.keys()}

            for fset in old_features.keys():
                #subsum = 0
                for feat, freq in old_features[fset].items():
                    if feat in filter[fset]:
                        new_features[fset][feat] = freq
                        #subsum += count
                        if freq > max[fset] and feat!='<BIAS>':
                            max[fset] = freq
                            #print(fset, feat, freq)
                        if freq < min[fset]:
                            min[fset] = freq

                # Convert counts to frequencies
                #for feat in new_features[fset].keys():
                #    new_features[fset][feat] /= subsum

                # Add bias to fset
                if not '<BIAS>' in new_features[fset].keys():
                    print('Added bias to ', fset)
                    new_features[fset]['<BIAS>'] = 1
                #if not self.filter:
                    #print('----------------------------------------')
                    #print(old_features)
                    #print('---------------------------------------->>')
                    #print(new_features)
                    #print('----------------------------------------')

            #del old_features
            sample.set_features(new_features)

            #print('max:')
            #print(max)
            #print('min')
            #print(min)


    def extract_features(self, tweet):

        # Dict of dicts. First keys are feature sets. Second keys are features.
        tokens = Tokenizer().get_tokens(tweet.get_text(), **self.token_options)
        features = {fset:{} for fset in self.feature_sets}

        if not tokens:
            print('WARNING NONETYPE TOKENS: ', tweet.get_text())
            return features

        # Extract token unigrams
        if '1GR' in self.feature_sets:
            for token in tokens:
                unigram = token[0]
                if self.filter:
                    if unigram in self.filter['1GR']:
                        features['1GR'][unigram] = features['1GR'].get(unigram,0)+1
                else:
                    features['1GR'][unigram] = features['1GR'].get(unigram,0)+1
                #self.feature_labels['1GR'][unigram] = self.feature_labels['1GR'].get(unigram,0)+1

        # If we need to extract token multi-grams
        if any([1 if fset in ('2GR','3GR', '4GR-S1') else 0 \
            for fset in self.feature_sets]):
            # Get "strict" tokens, i.e. without replacing anything
            tp = self.token_options.copy()
            tp['stem'] = False
            tp['replace_emojis'] = False
            tp['replace_num'] = False
            tp['addit_mode'] = False
            strict_tokens = Tokenizer().get_tokens(tweet.get_text(), **tp)

            # Extract bigrams
            if '2GR' in self.feature_sets and len(strict_tokens)>1:
                bigrams = self.get_bigrams(strict_tokens)
                features['2GR'].update(bigrams)
            # Extract trigrams
            if ('3GR' in self.feature_sets or '3GR-S1' in self.feature_sets) \
                and len(strict_tokens)>2:
                trigrams, skip_trigrams = self.get_trigrams(strict_tokens)
                if '3GR' in self.feature_sets:
                    features['3GR'].update(trigrams)
                if '3GR-S1' in self.feature_sets:
                    features['3GR-S1'].update(skip_trigrams)
            # Extract skip-one-tetragrams
            if '4GR-S1' in self.feature_sets and len(strict_tokens)>3:
                tetragrams = self.get_tetragrams(strict_tokens)
                features['4GR-S1'].update(tetragrams)

        # Extract POS
        if 'POS' in self.feature_sets:
            plain_tokens = [t[0] for t in tokens]
            tokens_with_pos = pos_tag(plain_tokens)
            for token in tokens_with_pos:
                tag = '<' + token[1] + '>'
                if self.filter:
                    if tag in self.filter['POS']:
                        features['POS'][tag] = features['POS'].get(tag,0)+1
                else:
                    features['POS'][tag] = features['POS'].get(tag,0)+1
                #self.feature_labels['POS'][tag] = self.feature_labels['POS'].get(tag,0)+1


        # Extract character ngrams
        if any([1 if fset in ('1CH', '2CH', 'CH') else 0 \
            for fset in self.feature_sets]):

            text = tweet.get_text()

            if '1CH' in self.feature_sets:
                for ch in text:
                    if self.filter:
                        if ch in self.filter['1CH']:
                            features['1CH'][ch] = features['1CH'].get(ch,0)+1
                    else:
                        features['1CH'][ch] = features['1CH'].get(ch,0)+1
                    #self.feature_labels['1CH'][tag] = self.feature_labels['1CH'].get(ch,0)+1

            if '2CH' in self.feature_sets:
                i=2
                while(i<len(text)):
                    ch2 = text[i-2:i]
                    if self.filter:
                        if ch2 in self.filter['2CH']:
                            features['2CH'][ch2] = features['2CH'].get(ch2,0)+1
                    else:
                        features['2CH'][ch2] = features['2CH'].get(ch2,0)+1
                    #self.feature_labels['2CH'][ch2] = self.feature_labels['2CH'].get(ch2,0)+1
                    i+=1

            if '3CH' in self.feature_sets:
                i=3
                while(i<len(text)):
                    ch3 = text[i-3:i]
                    if self.filter:
                        if ch3 in self.filter['3CH']:
                            features['3CH'][ch3] = features['3CH'].get(ch3,0)+1
                    else:
                        features['3CH'][ch3] = features['3CH'].get(ch3,0)+1
                    #self.feature_labels['3CH'][ch3] = self.feature_labels['3CH'].get(ch3,0)+1
                    i+=1

        # Convert counts to frequencies
        for fset in features.keys():
            for f in features[fset].keys():
                # for characaters count the number of text
                if 'CH' in fset:
                    features[fset][f] /= len(tweet.get_text())
                # for ngrams/pos count number of tokens
                else:
                    features[fset][f] /= len(tokens)
            # Add bias to each feature set
            features[fset]['<BIAS>'] = 1
            #self.feature_labels[fset].add('<BIAS>')


        if 'SENTA' in self.feature_sets:
            features['SENTA'] = {}
            for fset in self.feature_sets:
                if fset not in ('SENTA', '1CH', '2CH', '3CH', 'POS'):
                    if self.filter:
                        if fset not in self.filter['SENTA']:
                            continue
                    tokens = list( features[fset].keys() )
                    try:
                        positive, negative = senti_classifier.polarity_scores(tokens)
                    except:
                        pass
                    else:
                        features['SENTA'][fset] = (negative - positive)
                features['SENTA']['<BIAS>'] = 1


        return features


    def get_bigrams(self, tokens):
        bigrams = {}
        prev = '<BEGIN>'  # Previous token
        for token in tokens:
            bigram = ' '.join([prev, token[0]])
            if self.filter:
                if bigram in self.filter['2GR']:
                    bigrams[bigram] = bigrams.get(bigram,0)+1
            else:
                bigrams[bigram] = bigrams.get(bigram,0)+1
            prev = token[0]
            #self.feature_labels['2GR'][bigram] = self.feature_labels['2GR'].get(bigram,0)+1
        bigram = ' '.join([prev, '<END>'])
        if self.filter:
            if bigram in self.filter['2GR']:
                bigrams[bigram] = bigrams.get(bigram,0)+1
        else:
            bigrams[bigram] = bigrams.get(bigram,0)+1

        #self.feature_labels['2GR'][bigram] = self.feature_labels['2GR'].get(bigram,0)+1
        return bigrams


    def get_trigrams(self, tokens):
        trigrams = {}
        skip_trigrams = {}
        prev = '<BEGIN>'
        prev_prev = None
        for token in tokens:
            if prev_prev:
                trigram = ' '.join([prev_prev, prev, token[0]])
                if self.filter:
                    if trigram in self.filter['3GR']:
                        trigrams[trigram] = trigrams.get(trigram,0)+1
                else:
                    trigrams[trigram] = trigrams.get(trigram,0)+1
                #self.feature_labels['3GR'][trigram] = self.feature_labels['3GR'].get(trigram,0)+1
            prev_prev = prev
            prev = token[0]
        trigram = ' '.join([prev_prev, prev, '<END>'])
        if self.filter:
            if trigram in self.filter['3GR']:
                trigrams[trigram] = trigrams.get(trigram,0)+1
        else:
            trigrams[trigram] = trigrams.get(trigram,0)+1

        if '3GR-S1' in self.feature_sets:
            for t in trigrams.keys():
                st = list(t)
                st[1] = '<SKIP>'
                st = ' '.join(st)
                if self.filter:
                    if st in self.filter['3GR-S1']:
                        skip_trigrams[st] = skip_trigrams.get(st,0)+1
                else:
                    skip_trigrams[st] = skip_trigrams.get(st,0)+1
        #self.feature_labels['3GR'][trigram] = self.feature_labels['3GR'].get(trigram,0)+1

        return trigrams, skip_trigrams


    def get_tetragrams(self, tokens):
        """
        Tetragrams with skip points!
        """
        tetragrams = []  # Simple tetragrams, list of lists
        skip_tetr = {} # Tetragrams with skip
        prev = None
        prev_prev = None
        prev_prev_prev = None
        for token in tokens:
            if prev_prev_prev:
                t = [prev_prev_prev, prev_prev, prev, token[0]]
                tetragrams.append(t)
            prev_prev_prev = prev_prev
            prev_prev = prev
            prev = token[0]
        for t in tetragrams:
            skip_a = list(t)
            skip_b = list(t)
            # Replace 2nd and 3d tokens with skip
            skip_a[1] = '<SKIP>'
            skip_b[2] = '<SKIP>'
            skip_a = ' '.join(skip_a)
            skip_b = ' '.join(skip_b)
            if self.filter:
                if skip_a in self.filter['4GR-S1']:
                    skip_tetr[skip_a] = skip_tetr.get(skip_a,0)+1
                if skip_b in self.filter['4GR-S1']:
                    skip_tetr[skip_b] = skip_tetr.get(skip_b,0)+1
            else:
                skip_tetr[skip_a] = skip_tetr.get(skip_a,0)+1
                skip_tetr[skip_b] = skip_tetr.get(skip_b,0)+1
            #self.feature_labels['4GR-S1'][skip_a] = self.feature_labels['4GR-S1'].get(skip_a,0)+1
            #self.feature_labels['4GR-S1'][skip_b] = self.feature_labels['4GR-S1'].get(skip_b,0)+1
        return skip_tetr


    def get_feature_labels(self):
        if self.feature_labels:
            return self.feature_labels
        return None
