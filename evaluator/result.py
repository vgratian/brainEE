import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class Result():
    """
    This class acts as an interface to display Evaluation scores on the terminal
    or write them into a file. A header is printed when the method show() is
    called for the first time. (header is the list of all scores that will be shown).

    Subesequently, each time the method show() is used, a new line of pr_scores
    will be printed. Nothing is printed if the write() method is called
    """

    def __init__(self, nodes, params):

        self.params = params
        self.nodes = nodes
        self.convergence = []
        self.fscores = {n:[] for n in self.nodes}
        self.fscores['ensemble'] = []
        self.highest = 0
        self.epochs = 0

        if self.params['print_results']:
            self.print_header()
        if self.params['save_results']:
            self.print_header(write=True)


    def draw_graph(self, save=False):
        #try:
        fig, ax = plt.subplots(figsize=(14, 10))

        ax.plot(self.fscores['ensemble'],  label='Model F-score (test data)',linewidth=3.0)
        #ax.plot(self.convergence, label='Learning curve (accuracy on train data)',linewidth=0.4)
        for m in self.fscores.keys():
            if m!='ensemble':
                ax.plot(self.fscores[m], label='{} F'.format(m), linewidth=0.4)
        #ax.legend(loc='best')
        ax.legend().set_visible(False)
        plt.axis([0, self.epochs, 0, 1])
        #tp = ', '.join([str(p) for p in token_params if token_params[p]]) if token_params else 'None'
        plt.suptitle('BrainEE ∙ "{}"'.format(self.params['model_name']), y=.96, fontsize=14)
        plt.title('Nodes: {}, Killed nodes: {}, Epochs: {}, Dataset {} / {}. ' \
            ' Voting: {}.'.format(  len(self.nodes),
                                  ( self.params['node_count'] - len(self.nodes)),
                                    self.params['epochs'],
                                    self.params['data_size']['train'],
                                    self.params['data_size']['test'],
                                    self.params['vote_method'] ), fontsize=10)

        plt.xlabel('Epochs')
        plt.ylabel('Model F-score / Convergence')
        if save:
            fp = self.params['save_in_directory'] + self.params['model_name'] + '.png'
            plt.savefig(fp, bbox_inches='tight')
            print('Plot saved as [{}]'.format(fp))
        else:
            plt.show()
        #except:
        #    print('WARNING: Unable to print or save plot. Skipping.')
        #else:
        #    pass


    def remove_node(self, node):

        if node in self.nodes:
            self.nodes.remove(node)
        else:
            print('WARNING: Trying to remove node [] that is not in list.'.format(node))

        if self.params['print_results']:
            self.print_header()
        if self.params['save_results']:
            self.print_header(write=True)


    def print_header(self, write=False):

        BOLD = '\033[1m' if not write else ''
        UNBOLD = '\033[0m' if not write else ''

        name = 'ENSEMBLE'
        header1 = '{b}{ensemble}\t\t{nodes}{unb}'.format \
            (
                b = BOLD,
                ensemble = name + ( ' ' * (82 - len(name) ) ),
                nodes = '\t'.join([ ( n + ' ' * (12 - len(n)) ) for n in self.nodes]),
                unb = UNBOLD
            )
        header2 = 'Conv    {b}eFmac   eFmic    ePrec  eRec{unb}    anger      sad        happy      other      .\t{node_headers}'.format \
            (
                b = BOLD,
                unb = UNBOLD,
                node_headers = '\tFmac  nFmic' * len(self.nodes)
            )
        if not write:
            print()
            print(header1)
            print(header2)
        else:
            fp = self.params['save_in_directory'] + self.params['model_name'] + '.scores'
            with open(fp, 'a') as f:
                f.write('\n' + header1 + '\n' + header2 + '\n')


    def print_header_old(self):
        print()
        name = 'ENSEMBLE'
        header1 = '{b}{ensemble}\t\t{nodes}{unb}'.format \
            (
                b = '\033[1m',
                ensemble = name + ( ' ' * (82 - len(name) ) ),
                nodes = '\t'.join([ ( n + ' ' * (60 - len(n)) ) for n in self.nodes]),
                unb = '\033[0m'
            )
        print(header1)
        header2 = 'Conv    {b}eFmac   eFmic    ePrec  eRec{unb}    anger      sad        happy      other      .\t{node_headers}'.format \
            (
                b = '\033[1m',
                unb = '\033[0m',
                node_headers = '\t\033[1mnFmac   nFmic\033[0m   aP   aR     sP   sR     hP   hR     oP   oR' * len(self.nodes)
            )
        print(header2)


    def show(self, score, accuracy=None):
        self.convergence.append(accuracy)
        for model in score.models:
            self.fscores[model].append(score.f_micro[model])
        self.epochs += 1

        highlight = False
        if score.f_macro['ensemble'] > self.highest:
            self.highest = score.f_macro['ensemble']
            highlight = True
        print(score.__str__(accuracy, highlight=highlight))
        # print(score(accuracy))


    def write(self, score, accuracy=None):
        fp = self.params['save_in_directory'] + self.params['model_name'] + '.scores'
        with open(fp, 'a') as f:
            f.write(score.__str__(accuracy, use_bold=False) + '\n')
        #print('Evaluation results saved as [{}]'.format(fp))
