
class Scorer():

    def __init__(self, corpus, nodes):

        self.corpus = corpus
        self.classes = corpus.get_labels()
        self.nodes = nodes
        self.models = nodes + ['ensemble']

        self.true_positives = {m:{c:0 for c in self.classes} for m in self.models}
        self.false_positives = {m:{c:0 for c in self.classes} for m in self.models}
        self.false_negatives = {m:{c:0 for c in self.classes} for m in self.models}
        self.get_base_scores()

        #print('=======')
        #print('E TP', self.true_positives['ensemble'])
        #print('E FP', self.false_positives['ensemble'])
        #print('E FN', self.false_negatives['ensemble'])
        #print('-------')
        self.precision = {m:0 for m in self.models}
        self.recall = {m:0 for m in self.models}

        self.precision_labels = {m:{c:0 for c in self.classes} for m in self.models}
        self.recall_labels = {m:{c:0 for c in self.classes} for m in self.models}

        self.f_macro = {m:0 for m in self.models}
        self.f_micro = {m:0 for m in self.models}
        self.f_micro_labels = {m:{c:0 for c in self.classes} for m in self.models}
        self.get_f_scores()

        #print('E P', self.precision['ensemble'])
        #print('E R', self.recall['ensemble'])
        #print('-------')


        #print('EL P', self.precision_labels['ensemble'])
        #print('EL R', self.recall_labels['ensemble'])
        #print('-------')


        #print('E mac', self.f_macro['ensemble'])
        #print('E mic', self.f_micro['ensemble'])
        #print('E macL', self.f_micro_labels['ensemble'])
        #print('=======')


    def get_base_scores(self):
        """
        Calculate TP, FP, FN, Precision and Recall scores for all labels.
        Adds to the corresponding dictionaries, no return data.
        """
        # Get predictions from corpus to calculate TP, FP, FN
        for x in self.corpus:
            votes = x.get_votes()
            gold_label = x.get_gold_label()
            for m in self.models:
                prediction = x.get_pred_label() if m=='ensemble' else votes[m]
                for c in self.classes:
                    # Calculate scores for the ensemble model
                    if c == prediction:
                        if prediction == gold_label:
                            self.true_positives[m][c] += 1
                        else:
                            self.false_positives[m][c] +=1
                    elif c == gold_label:
                        self.false_negatives[m][c] +=1


    def get_f_scores(self):
        for m in self.models:
            tp = sum([self.true_positives[m][c] for c in self.true_positives[m] if c !='others'])
            fp = sum([self.false_positives[m][c] for c in self.false_positives[m] if c!='others'])
            fn = sum([self.false_negatives[m][c] for c in self.false_negatives[m] if c!='others'])

            p = ( tp / (tp+fp) ) if (tp+fp)!=0 else 0
            r = ( tp / (tp+fn) ) if (tp+fn)!=0 else 0

            self.precision[m] = p
            self.recall[m] = r
            self.f_micro[m] = ( (2*p*r)/(p+r) ) if (p+r)!=0 else 0

            f_sum = 0

            for c in self.classes:
                tp = self.true_positives[m][c]
                fp = self.false_positives[m][c]
                fn = self.false_negatives[m][c]

                p = (tp / (tp+fp)) if (tp+fp)!=0 else 0
                r = (tp / (tp+fn)) if (tp+fn)!=0 else 0

                self.precision_labels[m][c] = p
                self.recall_labels[m][c] = r

                self.f_micro_labels[m][c] = ( (2*p*r)/(p+r)) if (p+r)!=0 else 0

                if c!='others':
                    f_sum += self.f_micro_labels[m][c]

            self.f_macro[m] = f_sum / (len(self.classes) - 1)


    def __str__(self, convergence='--', use_bold=True, highlight=False):
        """
        Fancy list of macro, micro f-scores and precision and recall for all labels
        """

        size = 88

        eFmac = str(round(self.f_macro['ensemble'], 3))
        eFmic = str(round(self.f_micro['ensemble'], 3))
        eP = str(round(self.precision['ensemble'], 2))
        eR = str(round(self.recall['ensemble'], 2))
        aP = str(round(self.precision_labels['ensemble']['angry'], 2))
        aR = str(round(self.recall_labels['ensemble']['angry'], 2))
        sP = str(round(self.precision_labels['ensemble']['sad'], 2))
        sR = str(round(self.recall_labels['ensemble']['sad'], 2))
        hP = str(round(self.precision_labels['ensemble']['happy'], 2))
        hR = str(round(self.recall_labels['ensemble']['happy'], 2))
        oP = str(round(self.precision_labels['ensemble']['others'], 2))
        oR = str(round(self.recall_labels['ensemble']['others'], 2))

        # Bold unbold
        b = ''
        unb = ''

        if use_bold:
            b = '\033[1m'
            unb = '\033[0m'
            size += 8

        if highlight:
            eFmac = '\033[7m' + eFmac + '\033[0m'
            size += 8

        scores = '{conv}    {b}{eFmac}   {eFmic}    {eP}   {eR}{unb}    {aP} {aR}  {sP} {sR}  {hP} {hR}  {oP} {oR}\t'.format \
            (
                conv = str(convergence) + ( (' ' * (4 - len(str(convergence)))) ),
                b = b,
                eFmac = eFmac + (' ' * (5 - len(eFmac))),
                eFmic = eFmic + (' ' * (5 - len(eFmic))),
                eP = eP + (' ' * (4 - len(eP))),
                eR = eR + (' ' * (4 - len(eR))),
                unb = unb,
                aP = aP + (' ' * (4 - len(aP))),
                aR = aR + (' ' * (4 - len(aR))),
                sP = sP + (' ' * (4 - len(sP))),
                sR = sR + (' ' * (4 - len(sR))),
                hP = hP + (' ' * (4 - len(hP))),
                hR = hR + (' ' * (4 - len(hR))),
                oP = oP + (' ' * (4 - len(oP))),
                oR = oR + (' ' * (4 - len(oR))),
            )
        scores += ( ' ' * (size - len(scores)) )

        for n in self.nodes:

            nFmac = str(round(self.f_macro[n], 3))
            nFmic = str(round(self.f_micro[n], 3))

            node = '\t{nFmac}  {nFmic}'.format \
                (
                    nFmac = nFmac + (' ' * (5 - len(nFmac))),
                    nFmic = nFmic + (' ' * (5 - len(nFmic)))
                )
            scores += node
            scores += ( ' ' * (12 - len(node)) )

        return scores


    def __str__old(self, convergence='--', use_bold=True):
        """
        Fancy list of macro, micro f-scores and precision and recall for all labels
        """

        eFmac = str(round(self.f_macro['ensemble'], 3))
        eFmic = str(round(self.f_micro['ensemble'], 3))
        eP = str(round(self.precision['ensemble'], 2))
        eR = str(round(self.recall['ensemble'], 2))
        aP = str(round(self.precision_labels['ensemble']['angry'], 2))
        aR = str(round(self.recall_labels['ensemble']['angry'], 2))
        sP = str(round(self.precision_labels['ensemble']['sad'], 2))
        sR = str(round(self.recall_labels['ensemble']['sad'], 2))
        hP = str(round(self.precision_labels['ensemble']['happy'], 2))
        hR = str(round(self.recall_labels['ensemble']['happy'], 2))
        oP = str(round(self.precision_labels['ensemble']['others'], 2))
        oR = str(round(self.recall_labels['ensemble']['others'], 2))


        scores = '{conv}    {b}{eFmac}   {eFmic}    {eP}   {eR}{unb}    {aP} {aR}  {sP} {sR}  {hP} {hR}  {oP} {oR}\t'.format \
            (
                conv = str(convergence) + ( (' ' * (4 - len(str(convergence)))) ),
                b = '\033[1m' if use_bold else '',
                eFmac = eFmac + (' ' * (5 - len(eFmac))),
                eFmic = eFmic + (' ' * (5 - len(eFmic))),
                eP = eP + (' ' * (4 - len(eP))),
                eR = eR + (' ' * (4 - len(eR))),
                unb = '\033[0m' if use_bold else '',
                aP = aP + (' ' * (4 - len(aP))),
                aR = aR + (' ' * (4 - len(aR))),
                sP = sP + (' ' * (4 - len(sP))),
                sR = sR + (' ' * (4 - len(sR))),
                hP = hP + (' ' * (4 - len(hP))),
                hR = hR + (' ' * (4 - len(hR))),
                oP = oP + (' ' * (4 - len(oP))),
                oR = oR + (' ' * (4 - len(oR))),
            )
        scores += ( ' ' * (88 - len(scores) + 8) )

        for n in self.nodes:

            nFmac = str(round(self.f_macro[n], 3))
            nFmic = str(round(self.f_micro[n], 3))
            aP = str(round(self.precision_labels[n]['angry'], 2))
            aR = str(round(self.recall_labels[n]['angry'], 2))
            sP = str(round(self.precision_labels[n]['sad'], 2))
            sR = str(round(self.recall_labels[n]['sad'], 2))
            hP = str(round(self.precision_labels[n]['happy'], 2))
            hR = str(round(self.recall_labels[n]['happy'], 2))
            oP = str(round(self.precision_labels[n]['others'], 2))
            oR = str(round(self.recall_labels[n]['others'], 2))

            node = '\t{b}{nFmac}   {nFmic}{unb}   {aP} {aR}   {sP} {sR}   {hP} {hR}   {oP} {oR}'.format \
                (   b = '\033[1m' if use_bold else '',
                    nFmac = nFmac + (' ' * (5 - len(nFmac))),
                    nFmic = nFmic + (' ' * (5 - len(nFmic))),
                    unb = '\033[0m' if use_bold else '',
                    aP = aP + (' ' * (4 - len(aP))),
                    aR = aR + (' ' * (4 - len(aR))),
                    sP = sP + (' ' * (4 - len(sP))),
                    sR = sR + (' ' * (4 - len(sR))),
                    hP = hP + (' ' * (4 - len(hP))),
                    hR = hR + (' ' * (4 - len(hR))),
                    oP = oP + (' ' * (4 - len(oP))),
                    oR = oR + (' ' * (4 - len(oR)))
                )
            scores += node
            scores += ( ' ' * (45 - len(node) + 8) )

        return scores
