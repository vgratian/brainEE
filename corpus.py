from question import Question
from random import shuffle

class Corpus(object):
    """ A datastructure to store Tweet objects.

        The files from which the data to store in Tweet/Corpus datastructure is
        read is assumed to have the following format:

            (Mandatory) The file containg the tweets and predicted labels: each
                line consists of two tab separated columns, in the first column
                the predicted label for this tweet and in the second the text of
                this tweet.

            (Optional) The file containg the gold labels: one label on each line
                no empty lines in between.

            Both files are assumed to have the same length and the lines in the
            files correspond to each other (e.g. The first line of the gold
            label file contains the label for the tweet on the first line of the
            other file).
    """

    def __init__(self, fp:str=None, name:str=None, print_distribution:bool=False):
        """ Inits the Corpus.

            Args:
                fp: filepath of the dataset (train or test).
                print_distribution: print statistics about class distribution.

            Dataset:
                expected to contain comma-serperated values, whith fields:
                    - Question ID
                    - Text
                    - Class (optional): 1 insincere, 0 sincere.
        """
        self.corpus = []
        self.caption = None
        self.name = name if name else None
        self.labels = set()
        self.curr = 0
        self.size = 0
        self.distribution = {}
        self.predictions_distribution = {}
        self.all_feature_names = set()
        if fp:
            self.read_dataset(fp)
        if print_distribution:
            self.print_distribution()


    def __iter__(self):
        return iter(self.corpus)


    def __next__(self):
        if self.curr >= self.size:
            raise StopIteration
        else:
            self.curr += 1
            return self.get_ith(self.curr - 1)


    def read_dataset(self, fp:str):
        with open(fp) as d:

            # first line is caption
            self.caption = d.readline().strip()
            caption = self.caption.split('\t')
            self.has_labels = True if len(caption)==5 else False

            # process each datapoint
            for line in d.readlines():
                try:
                    data = line.strip().split('\t')
                    #print(data)
                except:
                    print('Error parsing the following datapoint: [{}]'.format(line))
                else:
                    id = int(data[0])
                    text = ' <STOP> '.join(data[1:4])
                    label = None
                    if self.has_labels:
                        label = data[4]
                        if label not in self.labels:
                            self.labels.add(label)
                        self.distribution[label] = self.distribution.get(label,0) + 1
                    # add to corpus
                    x = Question(text=text, id=id, gold_label=label)
                    self.corpus.append(x)
                    self.size += 1


    def get_labels(self):
        return sorted(list(self.labels))


    def get_distribution(self):
        return self.distribution


    def get_size(self):
        return self.size


    def get_ith(self, i : int):
        """ Gets the i-th element in this corpus.
        """
        return self.corpus[i]


    def append(self, obj):
        self.corpus.append(obj)


    def shuffle(self):
        shuffle(self.corpus)


    def calculate_distribution(self, predictions=False):

        if not predictions:
            for x in self.corpus:
                label = x.get_gold_label()
                self.distribution[label] = self.distribution.get(label,0) + 1
        else:
            for x in self.corpus:
                label = x.get_pred_label()
                self.predictions_distribution[label] = self.predictions_distribution.get(label,0) + 1


    def print_distribution(self, predictions=False):

        #Exist if corpus has not labels
        if not self.has_labels:
            return None

        D = self.distribution

        if not predictions and not self.distribution:
            self.calculate_distribution()
            D = self.distribution

        elif predictions and not self.predictions_distribution:
            self.calculate_distribution(predictions=True)
            D = self.predictions_distribution

        print('\nDistribution of {} labels in {} data:'.format('predicted' \
            if predictions else 'gold', self.name))

        D['total'] = sum(D.values()) if D else 0
        labels = sorted(list(D.keys()))

        print('{}'.format('\t\t'.join(str(l) for l in labels)))

        all_data = []

        for l in labels:
            perc = round((D[l]*100 / D['total']),1) if D['total'] != 0 else 0
            data = '{}% ({})'.format(perc, D[l])
            all_data.append(data)
        print('{}\n'.format('\t'.join(all_data)))


    def write_predictions(self, params):
        fp = params['save_in_directory'] + params['model_name'] + '_ENSEMBLE.predictions'
        with open(fp, 'w') as f:
            if self.caption:
                f.write(self.caption + '\n')
            for x in self.corpus:
                id = str(x.get_id())
                if not x.get_text():
                    print('ERROR: recombining text: {}'.format(x))
                    text = '---\n'
                else:
                    text = x.get_text().split(' <STOP> ')
                    text = '\t'.join(text)
                label = x.get_pred_label()
                f.write('{}\t{}\t{}\n'.format(id, text, label))
        print('Ensemble predictions saved as [{}]'.format(fp))


    def write_node_predictions(self, params, nodes):
        fp = params['save_in_directory'] + params['model_name']

        for n in nodes:
            with open(fp+n+'.predictions', 'w') as f:
                if self.caption:
                    f.write(self.caption + '\n')
                for x in self.corpus:
                    id = str(x.get_id())
                    if not x.get_text():
                        print('ERROR: recombining text: {}'.format(x))
                        text = '---\n'
                    else:
                        text = x.get_text().split(' <STOP> ')
                        text = '\t'.join(text)
                    label = x.get_votes()[n]
                    f.write('{}\t{}\t{}\n'.format(id, text, label))
            print('{} Predictions saved as [{}]'.format(n, fp+n+'.predictions'))


if __name__ == '__main__':
    #c = Corpus('data/test_', 'test', print_distribution=True)
    c = Corpus('data/train_', 'train', print_distribution=True)
