

Ensemble of perceptrons for multi-class classification. This model was built for and evaluated on the [SemEval19 emotion prediction task](https://competitions.codalab.org/competitions/19790). BrainEE is an extension of [BrainT](https://github.com/ims-teamlab2018/Braint).

I tried to build the model in a way that is easily reusable. However everything is super-messy right now. I'll try to cleanup the project and add detailed documentation until end of February.
